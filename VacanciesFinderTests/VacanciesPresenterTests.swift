//
//  VacanciesPresenterTests.swift
//  VacanciesFinderTests
//
//  Created by Alexander Anosov on 16/11/2017.
//  Copyright © 2017 AnosovInc. All rights reserved.
//

import XCTest
@testable import VacanciesFinder

class DummyInteractor: VacanciesInteractorInputProtocol {
    var presenter: VacanciesInteractorOutputProtocol?
    var dataSource: VacanciesDataSourceInputProtocol?
    
    func startNewSearch() {
        print("startNewSearch")
    }
    
    func forceNewSearch() {
        print("forceNewSearch")
    }
    
    func retrieveVacancies() {
        print("retrieveVacancies")
        retrieveVacanciesCalled = true
    }
    
    var retrieveVacanciesCalled = false
}

class DummyView: VacanciesViewProtocol {
    var presenter: VacanciesPresenterProtocol?
    
    func isOutOfBounds() -> Bool {
        return true
    }
    
    func showTotalCount(_ count: Int) {
        
    }
    
    func showVacancies(_ vacancies: [Vacancy]) {
        
    }
    
    func showError() {
        
    }
    
    func showLoading() {
        
    }
    
    func hideLoading() {
        
    }
    
    
}

class VacanciesPresenterTests: XCTestCase {
    
    var dummyInteractor: DummyInteractor!
    var dummyView: DummyView!
    
    override func setUp() {
        super.setUp()
        dummyInteractor = DummyInteractor()
        dummyView = DummyView()
    }
    
    override func tearDown() {
        super.tearDown()
        
    }
    
    func testViewDidAppearCallsRetrieveVacancies() {
        let presenter = VacanciesPresenter()
        presenter.interactor = dummyInteractor
        dummyInteractor.retrieveVacanciesCalled = false
        presenter.viewDidAppear()
        XCTAssertTrue(dummyInteractor.retrieveVacanciesCalled)
    }
    
    func testViewGotOutOfBoundsDoesNotCallRetrieveVacanciesAfterDidLoadAll() {
        let presenter = VacanciesPresenter()
        presenter.interactor = dummyInteractor
        presenter.viewDidAppear()
        presenter.didLoadAll()
        dummyInteractor.retrieveVacanciesCalled = false
        presenter.viewGotOutOfBounds()
        XCTAssertFalse(dummyInteractor.retrieveVacanciesCalled)
    }
    
    func testViewDidPullToRefreshCallsRetrieveVacancies() {
        let presenter = VacanciesPresenter()
        presenter.interactor = dummyInteractor
        presenter.viewDidAppear()
        presenter.didLoadAll()
        dummyInteractor.retrieveVacanciesCalled = false
        presenter.viewDidPullToRefresh()
        XCTAssertTrue(dummyInteractor.retrieveVacanciesCalled)
    }
    
    func testDidRetrieveVacanciesCallsRetrieveVacanciesAfterViewDidAppear() {
        let presenter = VacanciesPresenter()
        presenter.interactor = dummyInteractor
        presenter.view = dummyView
        presenter.viewDidAppear()
        dummyInteractor.retrieveVacanciesCalled = false
        presenter.didRetrieveVacancies([])
        XCTAssertTrue(dummyInteractor.retrieveVacanciesCalled)
    }
    
    func testDidRetrieveVacanciesDoesNotCallRetrieveVacanciesAfterDidLoadAll() {
        let presenter = VacanciesPresenter()
        presenter.interactor = dummyInteractor
        presenter.view = dummyView
        presenter.viewDidAppear()
        presenter.didLoadAll()
        dummyInteractor.retrieveVacanciesCalled = false
        presenter.didRetrieveVacancies([])
        XCTAssertFalse(dummyInteractor.retrieveVacanciesCalled)
    }
    
    func testDidRetrieveVacanciesCallsRetrieveVacanciesAfterViewDidPullToRefresh() {
        let presenter = VacanciesPresenter()
        presenter.interactor = dummyInteractor
        presenter.view = dummyView
        presenter.viewDidAppear()
        presenter.didLoadAll()
        presenter.viewDidPullToRefresh()
        dummyInteractor.retrieveVacanciesCalled = false
        presenter.didRetrieveVacancies([])
        XCTAssertTrue(dummyInteractor.retrieveVacanciesCalled)
    }
}

//
//  VacancyEntityTests.swift
//  VacanciesFinderTests
//
//  Created by Alexandro on 13/11/2017.
//  Copyright © 2017 AnosovInc. All rights reserved.
//

import XCTest
@testable import VacanciesFinder

class VacancyEntityTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        
    }
    
    override func tearDown() {
        
        super.tearDown()
    }
    
    func testFromSalaryHasPrefixFrom() {
        let vacancy = VacancyEntity(name: "name", fromSalary: 1000, toSalary: nil, currency: "EUR")
        XCTAssertTrue(vacancy.salaryString.hasPrefix("From".localized), vacancy.salaryString)
    }
    
    func testToSalaryHasPrefixTo() {
        let vacancy = VacancyEntity(name: "name", fromSalary: nil, toSalary: 1000, currency: "EUR")
        XCTAssertTrue(vacancy.salaryString.hasPrefix("To".localized), vacancy.salaryString)
    }
    
    func testFromToSalaryHasDash() {
        let vacancy = VacancyEntity(name: "name", fromSalary: 500, toSalary: 1000, currency: "EUR")
        XCTAssertNotNil(vacancy.salaryString.range(of: " - "))
        XCTAssertNil(vacancy.salaryString.range(of: "From".localized))
        XCTAssertNil(vacancy.salaryString.range(of: "To".localized))
    }
    
    func testRURCurrency() {
        let vacancy = VacancyEntity(name: "name", fromSalary: nil, toSalary: 100000, currency: "RUR")
        XCTAssertTrue(vacancy.salaryString.hasSuffix("₽"))
    }
    
    func testUSDCurrency() {
        let vacancy = VacancyEntity(name: "name", fromSalary: 500, toSalary: 1000, currency: "USD")
        XCTAssertTrue(vacancy.salaryString.hasPrefix("$"), vacancy.salaryString)
    }
    
    func testEURCurrency() {
        let vacancy = VacancyEntity(name: "name", fromSalary: nil, toSalary: 1000, currency: "EUR")
        XCTAssertTrue(vacancy.salaryString.hasSuffix("€"))
    }
    
    func testSomeCurrencyCurrency() {
        let vacancy = VacancyEntity(name: "name", fromSalary: nil, toSalary: 100000, currency: "SomeCurrency")
        XCTAssertTrue(vacancy.salaryString.hasSuffix("100000"))
    }
}

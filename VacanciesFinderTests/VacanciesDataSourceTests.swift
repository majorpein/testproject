//
//  VacanciesDataSourceTests.swift
//  VacanciesFinderTests
//
//  Created by Alexander Anosov on 15/11/2017.
//  Copyright © 2017 AnosovInc. All rights reserved.
//

import XCTest
@testable import VacanciesFinder

class DummyLocalDataSource: VacanciesLocalDataSourceInputProtocol {
    func retrieveVacancies(urlString: String) -> VacanciesResponse? {
        let vacanciesResponse = VacanciesResponse(urlString: "test", totalCount: 10, totalPages: 1, vacancies: [])
        retrieveVacanciesCalled = true
        return vacanciesResponse
    }
    
    func cacheResponse(_ response: VacanciesResponse) {
        
    }
    
    func clearCaches() {
        
    }
    
    var retrieveVacanciesCalled = false
}

class VacanciesDataSourceTests: XCTestCase {
    
    var dataSource: VacanciesDataSource!
    var dummyLocalDataSource: DummyLocalDataSource!
    
    override func setUp() {
        super.setUp()
        
        dataSource = VacanciesDataSource()
        dummyLocalDataSource = DummyLocalDataSource()
        
        dataSource.localSource = dummyLocalDataSource
    }
    
    override func tearDown() {
        
        super.tearDown()
    }
    
    func testDidLoadAllIsFalseAfterStartNewSeacrh() {
        dataSource.startNewSearch()
        XCTAssertNotEqual(dataSource.status, .didLoadAll)
    }
    
    func testDidLoadAllIsTrueWhenLoadedLastPage() {
        dataSource.startNewSearch()
        dataSource.retrieveVacancies()
        XCTAssertEqual(dataSource.status, .didLoadAll)
    }
    
    func testRetrieveVacanciesAsksForDataAfterStartNewSeacrh() {
        dataSource.startNewSearch()
        dummyLocalDataSource.retrieveVacanciesCalled = false
        dataSource.retrieveVacancies()
        XCTAssertTrue(dummyLocalDataSource.retrieveVacanciesCalled)
    }
    
    func testRetrieveVacanciesDoesNotAskForDataWhenEverythingIsLoaded() {
        dataSource.startNewSearch()
        dataSource.retrieveVacancies()
        dummyLocalDataSource.retrieveVacanciesCalled = false
        dataSource.retrieveVacancies()
        XCTAssertFalse(dummyLocalDataSource.retrieveVacanciesCalled)
    }
}

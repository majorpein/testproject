//
//  RootWireframe.swift
//  VacanciesFinder
//
//  Created by Alexander Anosov on 16/11/2017.
//  Copyright © 2017 AnosovInc. All rights reserved.
//

import UIKit

protocol RootWireframeProtocol: class {
    func addRootViewController(to window: UIWindow?)
    var navigationController: UINavigationController? { get }
}

class RootWireframe: RootWireframeProtocol {
    
    var navigationController: UINavigationController?
    
    func addRootViewController(to window: UIWindow?) {
        navigationController = UINavigationController()
        window?.rootViewController = navigationController
    }
}

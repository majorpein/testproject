//
//  VacanciesCoreDataSource.swift
//  VacanciesFinder
//
//  Created by Alexander Anosov on 14/11/2017.
//  Copyright © 2017 AnosovInc. All rights reserved.
//

import UIKit
import CoreData

class VacanciesCoreDataSource: VacanciesLocalDataSourceInputProtocol {

    var appDelegate: AppDelegate? {
        return UIApplication.shared.delegate as? AppDelegate
    }
    
    var managedObjectContext: NSManagedObjectContext? {
        guard let delegate: AppDelegate = appDelegate else {
            return nil
        }
        return delegate.managedObjectContext
    }
    
    func logAllVacancies() {
        print("Logging all vacancies")
        
        guard let context = managedObjectContext else {
            print("Managed object context is nil")
            return
        }
        
        let vacanciesFetch = NSFetchRequest<NSFetchRequestResult>(entityName: "CDVacancyEntity")
        
        do {
            let fetchedVacancies = try context.fetch(vacanciesFetch) as! [CDVacancyEntity]
            
            for cdVacancyEntity in fetchedVacancies {
                print("Vacancy with name: \(cdVacancyEntity.name!)")
            }
        } catch let error as NSError {
            print("Failed to fetch CDVacanciesList: \(error.localizedDescription)")
        }
    }
    
    //MARK: VacanciesLocalDataSourceInputProtocol
    func retrieveVacancies(urlString: String) -> VacanciesResponse? {
        
        guard let context = managedObjectContext else {
            print("Managed object context is nil")
            return nil
        }
        
        let vacanciesListFetch = NSFetchRequest<NSFetchRequestResult>(entityName: "CDVacanciesList")
        let predicate = NSPredicate(format: "urlString = %@", urlString)
        vacanciesListFetch.predicate = predicate
        
        do {
            let fetchedVacanciesList = try context.fetch(vacanciesListFetch) as! [CDVacanciesList]
            
            guard let vacanciesList = fetchedVacanciesList.first else {
                print("CDVacanciesList is nil")
                return nil
            }
            
            guard let cdVacanciesSet = vacanciesList.vacancies else {
                print("cdVacanciesSet is nil")
                return nil
            }
            var vacancies: [VacancyEntity] = []
            for case let cdVacancyEntity as CDVacancyEntity in cdVacanciesSet.sorted(by: { ($0 as! CDVacancyEntity).index < ($1 as! CDVacancyEntity).index }) {
                let fromSalary = cdVacancyEntity.fromSalary == 0 ? nil : Int(cdVacancyEntity.fromSalary)
                let toSalary   = cdVacancyEntity.toSalary   == 0 ? nil : Int(cdVacancyEntity.toSalary)
                
                let vacancy = VacancyEntity(name: cdVacancyEntity.name!, fromSalary: fromSalary, toSalary: toSalary, currency: cdVacancyEntity.currency)
                vacancies.append(vacancy)
            }
            let vacanciesResponse = VacanciesResponse(urlString: vacanciesList.urlString!, totalCount: Int(vacanciesList.totalCount), totalPages: Int(vacanciesList.totalPages), vacancies: vacancies)
            return vacanciesResponse
        } catch let error as NSError {
            print("Failed to fetch CDVacanciesList: \(error.localizedDescription)")
        }
        return nil
    }
    
    func cacheResponse(_ response: VacanciesResponse) {
        guard let context = managedObjectContext else {
            print("Managed object context is nil")
            return
        }
        guard let listDescription = NSEntityDescription.entity(forEntityName: "CDVacanciesList", in: context) else {
            print("CDVacanciesList entity description is nil")
            return
        }
        
        let cdVacanciesList = CDVacanciesList(entity: listDescription, insertInto: context)
        cdVacanciesList.urlString  =       response.urlString
        cdVacanciesList.totalCount = Int64(response.totalCount)
        cdVacanciesList.totalPages = Int64(response.totalPages)
        
        var cdVacanciesSet: Set<CDVacancyEntity> = []
        for (index, vacancyEntity) in response.vacancies.enumerated() {
            guard let vacancyDescription = NSEntityDescription.entity(forEntityName: "CDVacancyEntity", in: context) else {
                print("CDVacancyEntity entity description is nil")
                continue
            }
            let cdVacancy = CDVacancyEntity(entity: vacancyDescription, insertInto: context)
            cdVacancy.index    =       Int16(index)
            cdVacancy.name     =       vacancyEntity.name
            cdVacancy.currency =       vacancyEntity.currency
            
            if let fromSalary = vacancyEntity.fromSalary {
                cdVacancy.fromSalary = Int64(fromSalary)
            }
            if let toSalary = vacancyEntity.toSalary {
                cdVacancy.toSalary = Int64(toSalary)
            }
            cdVacanciesSet.insert(cdVacancy)
        }
        cdVacanciesList.vacancies = NSSet(set: cdVacanciesSet)
        
        appDelegate?.saveContext()
    }
    
    func clearCaches() {
        guard let context = managedObjectContext else {
            print("Managed object context is nil")
            return
        }
        
        let vacanciesListFetch = NSFetchRequest<NSFetchRequestResult>(entityName: "CDVacanciesList")
        
        do {
            let results = try context.fetch(vacanciesListFetch)
            for case let cdVacanciesList as CDVacanciesList in results {
                context.delete(cdVacanciesList)
            }
            appDelegate?.saveContext()
        } catch let error as NSError {
            print("Detele all vacancies error : \(error.localizedDescription)")
        }
        logAllVacancies()
    }
}

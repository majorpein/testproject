//
//  VacanciesAPIDataSource.swift
//  VacanciesFinder
//
//  Created by Alexandro on 13/11/2017.
//  Copyright © 2017 AnosovInc. All rights reserved.
//

import Foundation

struct SalaryData: Codable {
    let from: Int?
    let to: Int?
    let currency: String?
}

struct VacancyData: Codable {
    let name: String
    let salary: SalaryData?
}

struct Response: Codable {
    let page: Int
    let found: Int
    let pages: Int
    let items: [VacancyData]?
}

class VacanciesAPIDataSource: VacanciesAPIDataSourceInputProtocol {
    
    func onError(_ msg: String) {
        remoteRequestHandler?.onError(msg)
    }
    
    func onErrorInMain(_ msg: String) {
        DispatchQueue.main.async { [weak self] in
            self?.onError(msg)
        }
    }
    
    //MARK: VacanciesAPIDataSourceInputProtocol
    weak var remoteRequestHandler: VacanciesAPIDataSourceOutputProtocol?
    
    func retrieveVacancies(urlString: String) {
        
        guard let url: URL = URL(string: urlString) else {
            onError("Error: couldn't create url")
            return
        }
        
        let session = URLSession.shared
        
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        
        let task = session.dataTask(with: request) { [weak self] (data, response, error) in
            
            guard let strongSelf = self else {
                self?.onErrorInMain("Error: API Data Source is nil")
                return
            }
            guard let responseData = data else {
                strongSelf.onErrorInMain("Error: did not receive data")
                return
            }
            
            guard error == nil else {
                strongSelf.onErrorInMain(error!.localizedDescription)
                return
            }
            
            let decoder = JSONDecoder()
            do {
                let response = try decoder.decode(Response.self, from: responseData)
                
                var vacancies = [VacancyEntity]()
                
                if let vacancyDataArray = response.items {
                    for vacancyData in vacancyDataArray {
                        var fromSalary: Int?
                        var toSalary: Int?
                        var currency: String?
                        if let salary = vacancyData.salary {
                            fromSalary = salary.from
                            toSalary = salary.to
                            currency = salary.currency
                        }
                        let vacancy = VacancyEntity(name: vacancyData.name, fromSalary: fromSalary, toSalary: toSalary, currency: currency)
                        
                        vacancies.append(vacancy)
                    }
                }
                
                DispatchQueue.main.async {
                    let vacanciesResponse = VacanciesResponse(urlString: urlString, totalCount: response.found, totalPages: response.pages, vacancies: vacancies)
                    strongSelf.remoteRequestHandler?.onRetrievedVacanciesResponse(vacanciesResponse)
                }
            } catch {
                strongSelf.onErrorInMain("error trying to convert data to JSON: " + error.localizedDescription)
            }
        }
        
        task.resume()
    }
    
    func cancelAllRequests() {
        URLSession.shared.getAllTasks { tasks in
            tasks.forEach { $0.cancel() }
        }
    }
}

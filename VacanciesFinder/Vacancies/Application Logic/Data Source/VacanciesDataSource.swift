//
//  VacanciesDataSource.swift
//  VacanciesFinder
//
//  Created by Alexander Anosov on 16/11/2017.
//  Copyright © 2017 AnosovInc. All rights reserved.
//

import Foundation

enum LoadingStatus {
    case isReady, isLoading, didLoadAll
}

class VacanciesDataSource: VacanciesDataSourceInputProtocol {
    //MARK: PROPERTIES
    let hardcodeUrlString = "https://api.hh.ru/vacancies?text=ios developer"
    var status: LoadingStatus = .isReady
    var page = 0
    var totalPages = 0
    var totalCount = 0
    
    var urlString: String? {
        let urlString = hardcodeUrlString + "&page=" + String(page)
        return urlString.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed)
    }
    //MARK: METHODS
    func retrievePortionOfVacancies() {
        
        guard let urlStringEncoded = urlString else {
            onError("Error: couldn't encode url string")
            return
        }
        
        if let vacanciesResponse = localSource?.retrieveVacancies(urlString: urlStringEncoded) {
            gotResponse(vacanciesResponse)
            return
        }
        
        status = .isLoading
        apiSource?.retrieveVacancies(urlString: urlStringEncoded)
    }
    
    func gotResponse(_ response: VacanciesResponse) {
        
        if totalCount != response.totalCount {
            totalCount = response.totalCount
            outputHandler?.onRetrievedTotalCount(totalCount)
        }
        totalPages = response.totalPages
        page += 1
        status = page == totalPages ? .didLoadAll : .isReady
        
        outputHandler?.onRetrievedVacancies(response.vacancies)
    }
    
    //MARK: VacanciesDataSourceInputProtocol
    weak var outputHandler: VacanciesDataSourceOutputProtocol?
    var apiSource: VacanciesAPIDataSourceInputProtocol?
    var localSource: VacanciesLocalDataSourceInputProtocol?
    
    func startNewSearch() {
        page = 0
        totalPages = 0
        totalCount = 0
        status = .isReady
        //retrievePortionOfVacancies()
    }
    
    func forceNewSearch() {
        localSource?.clearCaches()
        apiSource?.cancelAllRequests()
        startNewSearch()
    }
    
    func retrieveVacancies() {
        if status == .isReady {
            retrievePortionOfVacancies()
            print("Retrieving")
        }
    }
}

extension VacanciesDataSource: VacanciesAPIDataSourceOutputProtocol {
    //MARK: VacanciesAPIDataSourceOutputProtocol
    func onRetrievedVacanciesResponse(_ vacanciesResponse: VacanciesResponse) {
        localSource?.cacheResponse(vacanciesResponse)
        gotResponse(vacanciesResponse)
    }
    
    func onError(_ msg: String) {
        print(msg)
        status = .isReady
        outputHandler?.onError()
    }
}

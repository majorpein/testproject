//
//  VacanciesInteractor.swift
//  VacanciesFinder
//
//  Created by Alexandro on 13/11/2017.
//  Copyright © 2017 AnosovInc. All rights reserved.
//

import Foundation

class VacanciesInteractor: VacanciesInteractorInputProtocol {
    //MARK: VacanciesInteractorInputProtocol
    weak var presenter: VacanciesInteractorOutputProtocol?
    var dataSource: VacanciesDataSourceInputProtocol?
    
    func startNewSearch() {
        dataSource?.startNewSearch()
    }
    
    func forceNewSearch() {
        dataSource?.forceNewSearch()
    }
    
    func retrieveVacancies() {
        dataSource?.retrieveVacancies()
    }
}

extension VacanciesInteractor: VacanciesDataSourceOutputProtocol {
    //MARK: VacanciesDataSourceOutputProtocol
    func onRetrievedTotalCount(_ count: Int) {
        presenter?.didRetrieveTotalCount(count)
    }
    
    func onRetrievedVacancies(_ vacancies: [VacancyEntity]) {
        var vacanciesForPresenter: [Vacancy] = []
        for vacancyEntity in vacancies {
            let vacancy = Vacancy(title: vacancyEntity.name, salary: vacancyEntity.salaryString)
            vacanciesForPresenter.append(vacancy)
        }
        presenter?.didRetrieveVacancies(vacanciesForPresenter)
    }
    
    func didLoadAll() {
        presenter?.didLoadAll()
    }
    
    func onError() {
        presenter?.onError()
    }
}

//
//  Vacancy.swift
//  VacanciesFinder
//
//  Created by Alexandro on 13/11/2017.
//  Copyright © 2017 AnosovInc. All rights reserved.
//

import Foundation

struct Vacancy {
    let title, salary: String
}

//
//  VacancyEntity.swift
//  VacanciesFinder
//
//  Created by Alexandro on 13/11/2017.
//  Copyright © 2017 AnosovInc. All rights reserved.
//

import Foundation

struct VacancyEntity {

    let name: String
    let fromSalary, toSalary: Int?
    let currency: String?
    
    var salaryString: String {
        var salary: String = ""
        if let from = fromSalary {
            salary = string(from: from)
        }
        if let to = toSalary {
            salary = salary.isEmpty ? "To".localized + " " + string(from: to) : salary + " - " + string(from: to)
        } else if !salary.isEmpty {
            salary = "From".localized + " " + salary
        }
        return salary
    }

    private func string(from salary: Int) -> String {
        let formatter = NumberFormatter()
        guard let unwrappedCurrency = currency else {
            return "\(salary)"
        }
        switch unwrappedCurrency {
        case "RUR":
            formatter.locale = Locale(identifier: "ru_RU")
        case "USD":
            formatter.locale = Locale(identifier: "en_US")
        case "EUR":
            formatter.locale = Locale(identifier: "de_DE")
        default:
            return "\(salary)"
        }
        formatter.numberStyle = .currency
        if let formattedSalary = formatter.string(from: NSNumber(value: salary)) {
            return formattedSalary
        }
        return ""
    }
}

//
//  VacanciesResponse.swift
//  VacanciesFinder
//
//  Created by Alexander Anosov on 14/11/2017.
//  Copyright © 2017 AnosovInc. All rights reserved.
//

import Foundation

struct VacanciesResponse {
    let urlString: String
    let totalCount, totalPages: Int
    let vacancies: [VacancyEntity]
}

//
//  VacanciesProtocols.swift
//  VacanciesFinder
//
//  Created by Alexandro on 13/11/2017.
//  Copyright © 2017 AnosovInc. All rights reserved.
//

import UIKit

protocol VacanciesViewProtocol: class {
    var presenter: VacanciesPresenterProtocol? { get set }
    
    //MARK: PRESENTER -> VIEW
    func isOutOfBounds() -> Bool
    
    func showTotalCount(_ count: Int)
    func showVacancies(_ vacancies: [Vacancy])
    func showError()
    
    func showLoading()
    func hideLoading()
}

protocol VacanciesWireframeProtocol: class {
    var rootWireframe: RootWireframeProtocol? { get set }
    
    static func presentVacanciesModule(from parentWireframe: RootWireframeProtocol, delegate: VacanciesModuleDelegateProtocol?, animated: Bool) -> VacanciesModuleInterfaceProtocol
    //MARK: PRESENTER -> WIREFRAME
    func dismiss(controller: UIViewController, animated: Bool)
}

protocol VacanciesPresenterProtocol: class {
    var view: VacanciesViewProtocol? { get set }
    var interactor: VacanciesInteractorInputProtocol? { get set }
    var wireframe: VacanciesWireframeProtocol? { get set }
    var moduleDelegate: VacanciesModuleDelegateProtocol? { get set }
    
    //MARK: VIEW -> PRESENTER
    func viewDidAppear()
    func viewDidPullToRefresh()
    func viewGotOutOfBounds()
}

protocol VacanciesInteractorInputProtocol: class {
    var presenter: VacanciesInteractorOutputProtocol? { get set }
    var dataSource: VacanciesDataSourceInputProtocol? { get set }
    
    //MARK: PRESENTER -> INTERACTOR
    func startNewSearch()
    func forceNewSearch()
    func retrieveVacancies()
}

protocol VacanciesInteractorOutputProtocol: class {
    //MARK: INTERACTOR -> PRESENTER
    func didRetrieveTotalCount(_ count: Int)
    func didRetrieveVacancies(_ vacancies: [Vacancy])
    func didLoadAll()
    func onError()
}

protocol VacanciesDataSourceInputProtocol: class {
    var outputHandler: VacanciesDataSourceOutputProtocol? { get set }
    var apiSource: VacanciesAPIDataSourceInputProtocol? { get set }
    var localSource: VacanciesLocalDataSourceInputProtocol? { get set }
    
    //MARK: INTERACTOR -> DATASOURCE
    func startNewSearch()
    func forceNewSearch()
    func retrieveVacancies()
}

protocol VacanciesDataSourceOutputProtocol: class {
    //MARK: DATASOURCE -> INTERACTOR
    func onRetrievedTotalCount(_ count: Int)
    func onRetrievedVacancies(_ vacancies: [VacancyEntity])
    func didLoadAll()
    func onError()
}

protocol VacanciesAPIDataSourceInputProtocol: class {
    var remoteRequestHandler: VacanciesAPIDataSourceOutputProtocol? { get set }
    
    //MARK: DATASOURCE -> APIDATASOURCE
    func retrieveVacancies(urlString: String)
    func cancelAllRequests()
}

protocol VacanciesAPIDataSourceOutputProtocol: class {
    //MARK: APIDATASOURCE -> DATASOURCE
    func onRetrievedVacanciesResponse(_ vacanciesResponse: VacanciesResponse)
    func onError(_ msg: String)
}

protocol VacanciesLocalDataSourceInputProtocol: class {
    //MARK: COREDATASOURCE -> APIDATASOURCE
    func retrieveVacancies(urlString: String) -> VacanciesResponse?
    func cacheResponse(_ response: VacanciesResponse)
    func clearCaches()
}

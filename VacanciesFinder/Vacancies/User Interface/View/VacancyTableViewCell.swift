//
//  VacancyTableViewCell.swift
//  VacanciesFinder
//
//  Created by Alexandro on 13/11/2017.
//  Copyright © 2017 AnosovInc. All rights reserved.
//

import UIKit

class VacancyTableViewCell: UITableViewCell {

    var titleLabel, salaryLabel: UILabel!
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        titleLabel = UILabel()
        salaryLabel = UILabel()
        
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        salaryLabel.translatesAutoresizingMaskIntoConstraints = false
        
        contentView.addSubview(titleLabel)
        contentView.addSubview(salaryLabel)
        
        let views = ["title":titleLabel!, "salary":salaryLabel!]
        let horString = "H:|-[title]-[salary]-|"
        let titleVerString = "V:|-[title]-|"
        let salaryVerString = "V:|-[salary]-|"
        
        let horConstraints = NSLayoutConstraint.constraints(withVisualFormat: horString, options: [], metrics: nil, views: views)
        let titleVerConstraints = NSLayoutConstraint.constraints(withVisualFormat: titleVerString, options: [], metrics: nil, views: views)
        let salaryVerConstraints = NSLayoutConstraint.constraints(withVisualFormat: salaryVerString, options: [], metrics: nil, views: views)
        
        NSLayoutConstraint.activate(horConstraints + titleVerConstraints + salaryVerConstraints)
        
        salaryLabel.setContentCompressionResistancePriority(UILayoutPriority(760.0), for: .horizontal)
        salaryLabel.setContentHuggingPriority(UILayoutPriority(260.0), for: .horizontal)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}

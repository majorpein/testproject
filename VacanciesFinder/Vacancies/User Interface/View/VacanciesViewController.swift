//
//  VacanciesViewController.swift
//  VacanciesFinder
//
//  Created by Alexandro on 13/11/2017.
//  Copyright © 2017 AnosovInc. All rights reserved.
//

import UIKit

let rowHeight: CGFloat = 44.0

enum VacanciesViewState {
    case undefined, isEmpty, isError, isValidData
}

class VacanciesViewController: UIViewController {
    //MARK: PROPERTIES
    var presenter: VacanciesPresenterProtocol?
    
    var tableView: UITableView!
    var errorLabel: UILabel!
    var emptyLabel: UILabel!
    var activityIndicatorView: UIActivityIndicatorView!
    var refreshControl: UIRefreshControl!
    var placeholderImage: UIImage?
    
    var vacanciesList: [Vacancy] = []
    var vacanciesTotalCount = 0
    
    var state: VacanciesViewState = .undefined
    //MARK: METHODS
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let placeholder = UIImage(named: "placeholder.png") {
            let width = placeholder.size.width
            placeholderImage = placeholder.resizableImage(withCapInsets: UIEdgeInsets(top: 0.0, left: width/2.0 - 1.0, bottom: 0.0, right: width/2.0))
        }
        
        addTableView()
        addErrorLabel()
        addEmptyLabel()
        addActivityIndicatorView()
        
        view.backgroundColor = UIColor.white
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        presenter?.viewDidAppear()
    }
    
    func setState(_ newState: VacanciesViewState) {
        
        switch newState {
        case .isEmpty:
            errorLabel.isHidden = true
            emptyLabel.isHidden = false
            tableView.isHidden  = false
        case .isError:
            errorLabel.isHidden = false
            emptyLabel.isHidden = true
            tableView.isHidden  = false
        case .isValidData:
            errorLabel.isHidden = true
            emptyLabel.isHidden = true
            tableView.isHidden  = false
        case .undefined:
            break
        }
        state = newState
    }
}

extension VacanciesViewController {
    //MARK: Views and Constraints
    func addView(_ viewToAdd: UIView) {
        viewToAdd.translatesAutoresizingMaskIntoConstraints = false
        viewToAdd.isHidden = true
        view.addSubview(viewToAdd)
    }
    
    func addCenterAndTopConstraints(to viewToAdd: UIView) {
        
        let constant: CGFloat = 50.0
        
        let centerConstraint = viewToAdd.centerXAnchor.constraint(equalTo: view.centerXAnchor)
        var topConstraint: NSLayoutConstraint
        if #available(iOS 11, *) {
            topConstraint = viewToAdd.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: constant)
        } else {
            topConstraint = viewToAdd.topAnchor.constraint(equalTo: view.topAnchor, constant: constant)
        }
        
        NSLayoutConstraint.activate([centerConstraint, topConstraint])
    }
    
    func addTableView() {
        tableView = UITableView()
        
        tableView.dataSource = self
        tableView.delegate = self
        tableView.allowsSelection = false
        tableView.tableFooterView = UIView()
        tableView.register(VacancyTableViewCell.self, forCellReuseIdentifier: "VacancyTableViewCell")
        tableView.register(VacancyPlaceholderTableViewCell.self, forCellReuseIdentifier: "VacancyPlaceholderTableViewCell")
        
        addRefreshControl()
        addView(tableView)
        
        let views = ["tableView" : tableView!]
        let horString = "H:|-0-[tableView]-0-|"
        let verString = "V:|-0-[tableView]-0-|"
        
        let horConstraints = NSLayoutConstraint.constraints(withVisualFormat: horString, options: .alignAllTop, metrics: nil, views: views)
        let verConstraints = NSLayoutConstraint.constraints(withVisualFormat: verString, options: .alignAllTop, metrics: nil, views: views)
        
        NSLayoutConstraint.activate(horConstraints + verConstraints)
    }
    
    func addRefreshControl() {
        refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(handleRefresh), for: .valueChanged)
        tableView.addSubview(refreshControl)
    }
    
    func addErrorLabel() {
        errorLabel = UILabel()
        
        errorLabel.text = "Error".localized
        
        addView(errorLabel)
        
        addCenterAndTopConstraints(to: errorLabel)
    }
    
    func addEmptyLabel() {
        emptyLabel = UILabel()
        
        emptyLabel.text = "Empty".localized
        
        addView(emptyLabel)
        
        addCenterAndTopConstraints(to: emptyLabel)
    }
    
    func addActivityIndicatorView() {
        activityIndicatorView = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        
        activityIndicatorView.hidesWhenStopped = true
        
        addView(activityIndicatorView)
        
        addCenterAndTopConstraints(to: activityIndicatorView)
    }
}

extension VacanciesViewController {
    //MARK: handleRefresh
    @objc func handleRefresh() {
        vacanciesList.removeAll()
        vacanciesTotalCount = 0
        presenter?.viewDidPullToRefresh()
    }
}

extension VacanciesViewController: VacanciesViewProtocol {
    //MARK: VacanciesViewProtocol
    func isOutOfBounds() -> Bool {
        guard let lastRow = tableView.indexPathsForVisibleRows?.last?.row else {
            print("Couldn't find last row")
            return false
        }
        let isOutOfBounds = lastRow > vacanciesList.count
        if isOutOfBounds {
            print("Out of bounds")
        }
        return isOutOfBounds
    }
    
    func showTotalCount(_ count: Int) {
        vacanciesTotalCount = count
        tableView.reloadData()
    }
    
    func showVacancies(_ vacancies: [Vacancy]) {
        
        let firstRow = vacanciesList.count
        let lastRow = firstRow + vacancies.count - 1
        
        vacanciesList.append(contentsOf: vacancies)
        
        if (vacanciesList.isEmpty) {
            setState(.isEmpty)
            tableView.reloadData()
        } else {
            setState(.isValidData)
            if lastRow > vacanciesTotalCount {
                tableView.reloadData()
                print("Reloading data")
            } else {
                var indexPaths: [IndexPath] = []
                for row in firstRow...lastRow {
                    indexPaths.append(IndexPath(row: row, section: 0))
                }
                tableView.reloadRows(at: indexPaths, with: .automatic)
                print("Reloading rows")
            }
        }
    }
    
    func showError() {
        setState(.isError)
        tableView.reloadData()
    }
    
    func showLoading() {
        activityIndicatorView.startAnimating()
    }
    
    func hideLoading() {
        activityIndicatorView.stopAnimating()
        if refreshControl.isRefreshing {
            refreshControl.endRefreshing()
        }
    }
}

extension VacanciesViewController: UITableViewDataSource {
    //MARK: UITableViewDataSource
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return rowHeight
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return rowHeight
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return max(vacanciesTotalCount, vacanciesList.count)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row < vacanciesList.count {
            let cell: VacancyTableViewCell = tableView.dequeueReusableCell(withIdentifier: "VacancyTableViewCell", for: indexPath) as! VacancyTableViewCell
            
            cell.titleLabel?.text  = vacanciesList[indexPath.row].title
            cell.salaryLabel?.text = vacanciesList[indexPath.row].salary
            
            return cell
        } else {
            let cell: VacancyPlaceholderTableViewCell = tableView.dequeueReusableCell(withIdentifier: "VacancyPlaceholderTableViewCell", for: indexPath) as! VacancyPlaceholderTableViewCell
            
            cell.placeholder.image = placeholderImage
            
            return cell
        }
    }
}

extension VacanciesViewController: UITableViewDelegate {
    //MARK: UITableViewDelegate
}

extension VacanciesViewController: UIScrollViewDelegate {
    //MARK: UIScrollViewDelegate
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if isOutOfBounds() {
            presenter?.viewGotOutOfBounds()
        }
    }
}



//
//  VacancyPlaceholderTableViewCell.swift
//  VacanciesFinder
//
//  Created by Alexander Anosov on 14/11/2017.
//  Copyright © 2017 AnosovInc. All rights reserved.
//

import UIKit

class VacancyPlaceholderTableViewCell: UITableViewCell {

    var placeholder: UIImageView!
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        placeholder = UIImageView()
        
        placeholder.translatesAutoresizingMaskIntoConstraints = false
        placeholder.contentMode = .scaleToFill
        placeholder.alpha = 0.5
        
        contentView.addSubview(placeholder)
        
        let views = ["placeholder":placeholder!]
        let horString = "H:|-0-[placeholder]-0-|"
        let verString = "V:|-0-[placeholder]-0-|"
        
        let horConstraints = NSLayoutConstraint.constraints(withVisualFormat: horString, options: [], metrics: nil, views: views)
        let verConstraints = NSLayoutConstraint.constraints(withVisualFormat: verString, options: [], metrics: nil, views: views)
        
        NSLayoutConstraint.activate(horConstraints + verConstraints)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

}

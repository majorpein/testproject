//
//  VacanciesPresenter.swift
//  VacanciesFinder
//
//  Created by Alexandro on 13/11/2017.
//  Copyright © 2017 AnosovInc. All rights reserved.
//

import UIKit

class VacanciesPresenter: VacanciesPresenterProtocol {
    weak var moduleDelegate: VacanciesModuleDelegateProtocol?
    var everythingIsLoaded = false
    
    func retrieveVacancies() {
        if !everythingIsLoaded {
            interactor?.retrieveVacancies()
        }
    }
    
    //MARK: VacanciesPresenterProtocol
    weak var view: VacanciesViewProtocol?
    var interactor: VacanciesInteractorInputProtocol?
    var wireframe: VacanciesWireframeProtocol?
    
    func viewDidAppear() {
        view?.showLoading()
        everythingIsLoaded = false
        interactor?.startNewSearch()
        interactor?.retrieveVacancies()
    }
    
    func viewDidPullToRefresh() {
        everythingIsLoaded = false
        interactor?.forceNewSearch()
        interactor?.retrieveVacancies()
    }
    
    func viewGotOutOfBounds() {
        retrieveVacancies()
    }
}

extension VacanciesPresenter: VacanciesInteractorOutputProtocol {
    //MARK: VacanciesInteractorOutputProtocol
    func didRetrieveTotalCount(_ count: Int) {
        view?.showTotalCount(count)
    }
    
    func didRetrieveVacancies(_ vacancies: [Vacancy]) {
        view?.hideLoading()
        view?.showVacancies(vacancies)
        if let outOfBounds = view?.isOutOfBounds() {
            if outOfBounds {
                retrieveVacancies()
            }
        }
    }
    
    func didLoadAll() {
        everythingIsLoaded = true
    }
    
    func onError() {
        view?.hideLoading()
        view?.showError()
    }
}

extension VacanciesPresenter: VacanciesModuleInterfaceProtocol {
    
    //MARK: VacanciesModuleInterfaceProtocol
}

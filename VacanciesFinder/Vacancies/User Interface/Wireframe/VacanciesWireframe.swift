//
//  VacanciesWireframe.swift
//  VacanciesFinder
//
//  Created by Alexandro on 13/11/2017.
//  Copyright © 2017 AnosovInc. All rights reserved.
//

import UIKit

class VacanciesWireframe: VacanciesWireframeProtocol {
    weak var rootWireframe: RootWireframeProtocol?
    
    static func presentVacanciesModule(from parentWireframe: RootWireframeProtocol, delegate: VacanciesModuleDelegateProtocol?, animated: Bool) -> VacanciesModuleInterfaceProtocol {
        
        let view = VacanciesViewController()
        
        parentWireframe.navigationController?.pushViewController(view, animated: animated)
        
        let presenter: VacanciesModuleInterfaceProtocol & VacanciesPresenterProtocol & VacanciesInteractorOutputProtocol = VacanciesPresenter()
        let interactor: VacanciesInteractorInputProtocol & VacanciesDataSourceOutputProtocol = VacanciesInteractor()
        let dataSource: VacanciesDataSourceInputProtocol & VacanciesAPIDataSourceOutputProtocol = VacanciesDataSource()
        let apiDataSource: VacanciesAPIDataSourceInputProtocol = VacanciesAPIDataSource()
        let localDataSource: VacanciesLocalDataSourceInputProtocol = VacanciesCoreDataSource()
        let wireframe: VacanciesWireframeProtocol = VacanciesWireframe()
        
        wireframe.rootWireframe = parentWireframe
        view.presenter = presenter
        presenter.view = view
        presenter.wireframe = wireframe
        presenter.interactor = interactor
        presenter.moduleDelegate = delegate
        interactor.presenter = presenter
        interactor.dataSource = dataSource
        dataSource.outputHandler = interactor
        dataSource.apiSource = apiDataSource
        dataSource.localSource = localDataSource
        apiDataSource.remoteRequestHandler = dataSource
        
        return presenter
    }
    
    func dismiss(controller: UIViewController, animated: Bool) {
        controller.dismiss(animated: animated, completion: nil)
    }
}
